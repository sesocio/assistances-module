// Routes
import AssistancesHomeRoutes from './src/modules/home/routes';
import AssistancesUserRoutes from './src/modules/user/routes'
import AssistancesRoutes from './src/modules/assistances/routes';

// Stores
import AssistancesStore from './src/modules/assistances/store';

export {
  AssistancesHomeRoutes,
  AssistancesUserRoutes,
  AssistancesRoutes,
  AssistancesStore
};