import 'jsdom-global/register';
import Vue from 'vue';
import Vuetify from 'vuetify';
import Wrapper from '../../../src/modules/core/Wrapper';

Vue.use(Vuetify);
Vue.use(Wrapper);
Vue.component('Wrapper', Wrapper);
Vue.config.devtools = false;