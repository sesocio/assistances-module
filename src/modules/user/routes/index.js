export default [
  {
    path: "/asistencias/datos",
    name: "assistance_additional_information",
    component: () => import(/* webpackChunkName: "AdditionalInformation" */ '../pages/AdditionalInformation'),
    title: "Additional Information",
    meta: { requiresAuth: true }
  }
];
