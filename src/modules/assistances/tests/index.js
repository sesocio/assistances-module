import { mount, createLocalVue  } from "@vue/test-utils";
import Vuex from 'vuex'
import Vuetify from 'vuetify';

const localVue = createLocalVue();
const vuetify = new Vuetify();
const mocks = { $t: key => key };

localVue.use(Vuex);

describe("Hero component", () => {
  let userState = {
    isLoggedIn: false
  };

  let dialogMock = jest.fn();

  let store = new Vuex.Store({
    modules: {
      user: {
        state: {},
        actions: {},
        namespaced: true
      },
      onboarding_dialog: {
        state: {},
        actions: {
          openOnboardingDialog: dialogMock
        },
        namespaced: true
      }
    }
  });

  /*
  let wrapper = mount(Hero, {
    store,
    localVue,
    vuetify,
    mocks: mocks
  });
  */
});