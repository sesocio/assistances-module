const terms = {
  "asistencia-para-tu-mascota-arg": {
    html: `<strong>ASISTENCIA MASCOTAS</strong><br>
    1. DESCRIPCIÓN DE LOS SERVICIOS PRESTADOS POR EXPERTA ASISTENCIA
    Los servicios son prestados por Experta Asistencia SAU CUIT 30-71671840-5<br>
    Para solicitar un servicio, el cliente deberá comunicarse al número telefónico 0800 222 0668.<br>
    Servicio operativo las 24 horas del día, los 365 días del año.<br><br>

    <strong>DEFINICIONES</strong><br>
    Para los efectos de la presentación de los servicios aquí detallados, se entenderá por:<br>
    a) ACCIDENTE: Todo acontecimiento que provoque daños corporales a la Mascota causado única y
    directamente por una causa externa, violenta, fortuita y evidente.<br>
    b) AREA DE COBERTURA: Argentina.<br>
    c) CAC: Centro de Atención al Cliente<br>
    d) CENTRO VETERINARIO: Centro o consultorio veterinario privado o público o centro de servicios
    relacionados con la mascota que Experta Asistencia designe como destino de la mascota ante la solicitud
    de los servicios.<br>
    e) CLIENTE: Toda persona física que tenga bajo su tutela a la mascota (perro o gato), siempre y cuando
    su inscripción a los Servicios este vigente, al momento de producirse la solicitud de estos.<br>
    f) DOMICILIO RESIDENCIAL: El domicilio habitual que el cliente declaró al momento de suscripción a los
    servicios o cualquier modificación que el cliente haya hecho del mismo. En caso de que la modificación
    haya sido en fecha posterior a la de contratación de los presentes servicios, el cliente deberá
    comunicarlo expresamente a Experta Asistencia.<br>
    g) ENFERMEDAD GRAVE: Evento de carácter mórbido y de gravedad tal que ponga el riesgo la vida, de
    causa no accidental contraído o manifestado por primera vez por la mascota y que requiera tratamiento
    por parte de un médico.<br>
    h) EVENTOS: se considera evento a la ocasión de un servicio determinado, los plazos son por año
    calendario.<br>
    i) EXCEDENTE: Monto a pagar por parte del cliente, cuando se supere el tope establecido en la
    cobertura.<br>
    j) MASCOTA: Perros o gatos, destinados a compañía o vigilancia que residan de forma permanente con
    el Cliente.<br>
    k) MONTO LÍMITE POR EVENTO: suma máxima cubierta por Experta Asistencia para cada evento.<br>
    m) SERVICIOS: Son las actividades, operaciones y funciones a cargo de Experta Asistencia relacionados
    con asistencia, cuya descripción, límites, alcances y condiciones se detallan en este documento.<br><br>

    <strong>SERVICIOS</strong><br>
    a) VETERINARIO DE URGENCIA A CAUSA DE ACCIDENTE
    Límite: 1 vez al año, hasta $ 2.500
    En caso de que la mascota requiera una consulta veterinaria a consecuencia de un accidente,
    Experta Asistencia cubrirá el costo de atención, hasta el monto límite por evento, en un centro
    veterinario autorizado y designado. El excedente del servicio, en caso de haberlo, quedará a
    cargo del cliente.<br>
    b) VACUNACIÓN<br>
    Límite: 2 vacunas al año hasta $1.500 (calendario veterinario)
    A solicitud del cliente, Experta Asistencia gestionará y cubrirá el costo de las vacunas de la
    mascota y su aplicación en un centro veterinario autorizado.<br>
    c) HOSPITALIZACION EN CASO DE INTERVENCION QUIRURGICA<br>
    Límite: Hasta 5 días al año, tope $2.500 anual, $500 por día.
    En caso de hospitalización por cirugía que requiera internación de 5 (cinco) días o más, Experta
    Asistencia brindará el servicio de guardería para la mascota hasta por 5 días, a partir del
    requerimiento del mismo por el cliente. El cliente deberá proveer el alimento de la mascota, y
    entregar fotocopia de las vacunas e información sobre aspectos de la condición física y el
    temperamento de esta.<br>
    Este servicio se prestará en la ciudad de domicilio residencial y hasta el monto límite por
    evento. El excedente del servicio, en caso de haberlo, quedará a cargo del cliente.<br>
    d) CHEQUEO GENERAL<br>
    Límite: Hasta 1 vez al año, tope $ 1.500
    A solicitud del cliente, Experta Asistencia cubrirá los gastos del chequeo general de la mascota,
    1 vez por año según el tope establecido.<br>
    e) CASTRACION<br>
    Límite: Único evento, tope $ 2.500
    A solicitud del cliente Experta Asistencia cubrirá los gastos de castración según el tope
    establecido.<br>
    f) INTERVENCION QUIRURGICA<br>
    Límite: Hasta 1 vez al año, tope $ 2.500
    A solicitud del cliente y en caso de necesitar una intervención quirúrgica Experta Asistencia le
    cubrirá el costo de la intervención quirúrgica hasta el monto límite por evento. Las mismas se
    realizarán en un centro veterinario autorizado y designado. Las intervenciones quirúrgicas
    cubiertas son:<br>
    <ul>
    <li>Cirugía urológica</li>
    <li>Cirugía digestiva</li>
    <li>Cirugía aparato reproductor</li>
    <li>Cirugía bucal</li>
    <li>Cirugía oftálmica</li>
    <li>Cirugía plástica y reparadora</li>
    <li>Cirugía óptica</li>
    </ul>
    El Servicio está sujeto a previa evaluación y autorización del equipo veterinario de Experta
    Asistencia. Servicio disponible superados los 90 días desde la contratación de los servicios. El
    excedente del servicio, en caso de haberlo, quedará a cargo del Cliente.
    g) CREMACIÓN<br>
    Límite: hasta $2.500. Único evento<br><br>

    A solicitud del cliente y como consecuencia del fallecimiento de la mascota, Experta Asistencia
    coordinará y cubrirá los costos de cremación hasta el monto limite por evento.<br>
    h) ASESORÍA<br>
    Servicio ilimitado.
    En caso de que el cliente requiriera orientación sobre padecimientos y/o cuidados de la
    mascota, consultas veterinarias, orientación por viajes, referencias veterinarias telefónica
    información sobre clínicas, guarderías, vacunas, etc. Experta Asistencia brindará, información
    general y respuestas a las consultas del beneficiario.<br>
    i) COORDINACIÓN Y DESCUENTO EN GUARDERIA<br>
    Servicio ilimitado en consultas.<br>
    En caso de que el cliente requiriera orientación sobre guarderías y descuento en las mismas,
    Experta Asistencia brindará información general y respuestas a las consultas.<br>
    j) REEMBOLSOS<br>
    Tope: 1 evento anual, $1.500 por evento traslado accidente.
    1 evento anual $1.500 por publicación extravío.
    En caso de que el cliente así lo requiriera, Experta Asistencia realizara el reembolso de gastos
    de traslado por accidente y por publicaciones de avisos de extravío.<br>
    k) PAGO DE RECOMPENSA POR EXTRAVIO DE MASCOTA<br>
    Tope: 1 evento anual, $3.000 por evento. Con denuncia policial. 90 días de carencia.
    En caso de que el cliente así lo requiriera Experta Asistencia realizara el reembolso del pago de
    recompensa por el extravío de la mascota.<br>
    l) BAÑO PARA LA MASCOTA<br>
    Tope: 1 evento anual, $2.500 por evento.<br>
    En caso de que el cliente así lo requiriera Experta Asistencia cubrirá 1 baño para la mascota.<br>
    m) ORIENTACION TELEFONICA Y ASESORAMIENTO LEGAL EN CASO DE DAÑOS A TERCEROS<br>
    Tope: Sin Límites.<br>
    En caso de que el cliente así lo requiriera Experta Asistencia brindará al beneficiario la asesoría
    solicitada.<br>
    2) OBLIGACIONES DEL CLIENTE<br>
    El cliente se obliga bajo el presente a:
    a) Comunicar a Experta Asistencia, tan pronto como tenga conocimiento, sobre toda clase de
    correspondencia, documentación, citas, notificaciones, reclamaciones o requerimientos, relacionados
    con cualquier caso que hubiese sufrido al amparo de alguna de las coberturas del presente.<br>
    b) Notificar cualquier cambio de domicilio residencial de inmediato a Experta Asistencia. A falta de dicha
    notificación, Experta Asistencia considerará al cliente como responsable de los costos y gastos incurridos
    a causa de una situación de asistencia.<br>
    c) Facilitar una jaula, correa, bozal o cualquier elemento necesario para el control y traslado del animal,
    al momento del requerimiento de un servicio.<br>
    d) En caso que la mascota sufra alguna situación que requiera la prestación de los servicios, deberá
    reportarlo al CAC de Experta Asistencia, al número telefónico 0800 222 0668, debiendo proporcionar al
    coordinador del servicio la siguiente información:<br>
    <ul>
    <li>Datos del cliente tales como el nombre completo</li>
    <li>DNI y domicilio residencial.</li>
    <li>Identificación de la mascota (especie, raza, nombre, edad, color, etc.).</li>
    <li>Lugar donde se encuentra y el número telefónico donde Experta Asistencia pueda contactar al cliente, así como todos los datos que el coordinador de la asistencia le solicite para localizarlo.</li>
    </ul>
    e) Descripción del problema y servicio solicitado.<br>
    Los servicios descriptos, configuran la única obligación a cargo de Experta Asistencia y en ningún caso
    reintegrara al cliente las sumas que éste hubiera abonado fuera de los servicios aquí mencionados y
    previamente autorizados por Experta Asistencia.<br>
    3) EXCLUSIONES GENERALES.<br>
    Experta Asistencia no estará obligado a la prestación de los servicios en las siguientes situaciones:<br>
    a) Cuando el cliente no otorgue información clara, precisa, completa y veraz para la identificación
    de la mascota.<br>
    b) Cuando el cliente incumpla cualesquiera de las obligaciones indicadas en este condicionado.<br>
    c) Cuando el cliente actuare de mala fe.<br>
    d) Cuando los servicios sean solicitados como consecuencia directa o indirecta de huelgas,
    guerra, invasión, actos de enemigos extranjeros, hostilidades (se haya declarado la guerra o
    no), rebelión, guerra civil, insurrección, terrorismo, pronunciamientos, manifestaciones,
    movimientos populares, radioactividad o cualquier otra causa de fuerza mayor.<br>
    e) Cuando el cliente solicite un servicio para una mascota que no sea aquella para la cual
    contrato el servicio.<br>
    f) Cuando los servicios sean solicitados para mascotas que no sean consideradas animales
  domésticos según los usos y costumbres o cuya tenencia en cautiverio se considere ilegal.`,
  },
  "asistencias-para-tu-hogar-arg": {
    html: `<strong>ASISTENCIA HOGAR</strong><br>
    1. DESCRIPCIÓN DE LOS SERVICIOS:<br>
    Los servicios son prestados por Experta Asistencia SAU CUIT 30-71671840-5
    Para solicitar un servicio, el cliente deberá comunicarse al número telefónico 0800 222 0668.<br>
    Servicio operativo las 24 horas del día, los 365 días del año.
    DEFINICIONES
    Para los efectos de la presentación de los servicios aquí detallados, se entenderá por:<br>
    a) CLIENTE: Titular contratante del servicio.<br>
    b) DOMICILIO RESIDENCIAL: Casa, departamento o unidad para uso habitacional que sea el domicilio
    permanente del cliente.
    La residencia declarada por el cliente para la eventual prestación de los servicios de asistencia
    deberá corresponder exclusivamente a una unidad de vivienda familiar.
    Los servicios de asistencia contratados serán única y exclusivamente brindados en el domicilio
    declarado por el cliente.
    Eventuales cambios de domicilio durante la vigencia del servicio deberán ser fehacientemente
    informados por el cliente, siempre con anterioridad a cualquier solicitud de asistencia.<br>
    c) EXCEDENTE: Monto a pagar por parte del cliente cuando se supere el tope establecido en la
    cobertura al momento de recibir el servicio.<br>
    d) SERVICIOS: Son las actividades, operaciones y funciones a cargo del provedor relacionados con
    asistencia, cuya descripción, límites, alcances y condiciones se detallan en este documento.<br>
    e) PROVEEDOR: Es Experta Asistencia SAU<br>
    f) REINTEGRO: Monto de dinero a entregar al cliente cuando hubiera abonado gastos en relación con
    los servicios cubiertos descriptos siempre y cuando hayan sido autorizados por Experta Asistencia
    antes de ser realizados. Los reintegros se realizarán en cuenta bancaria a designar por el cliente.<br>
    g) ROBO: Usurpación ilegítima utilizando la fuerza sobre las cosas, amenazas o violencia física en las
    personas.<br>
    h) EVENTO: Solución brindada por el prestador en relación con una solicitud de servicio. En caso de que
    el beneficiario solicitare una segunda solución para resolver la misma eventualidad, el proveedor
    podrá considerarlo como un evento adicional. Los plazos son por año calendario.<br>
    i) ACCIDENTE: Suceso imprevisto que altera la marcha normal o prevista de las cosas, especialmente el
    que causa daños a una persona u objeto.<br>
    j) URGENCIA: Es todo imprevisto que, no siendo emergencia, afecte las instalaciones de la vivienda,
    impidiendo su normal funcionamiento.<br>
    k) EMERGENCIA: Es el imprevisto que ocasione inhabitabilidad de la vivienda y/o eventual riesgo de
    vida de sus habitantes, y/o la imposibilidad de acceso o salida de esta.<br>
    l) AVERIA: Daño o deterioro de un objeto de modo tal que su funcionamiento normal esté impedido.<br>
    m) IDENTIFICACIÓN COMO CLIENTE: La identificación será necesaria al momento de ser solicitada la
    asistencia. En todos los casos, el beneficiario deberá suministrar la siguiente información:<br>
    Nombre y apellido<br>
    Número DNI<br>
    Motivo del llamado<br>
    Número telefónico para eventual contacto<br>
    n) VALIDEZ TERRITORIAL: Los servicios se brindarán en la República Argentina.<br>
    <strong>SERVICIOS</strong><br>
    a) EMERGENCIA.<br>
    Limitado a $ 1.500 por evento y 4 (cuatro) eventos anuales por cada especialidad.<br>
    Se proporcionará el servicio las 24 horas del día de los 365 días al año.<br>
    El tope de cobertura por evento incluye costos de mano de obra y materiales. El excedente, en caso de
    corresponder, correrá por cuenta del cliente.<br>
    Experta Asistencia enviará al domicilio residencial del beneficiario un prestador que se encargará de
    atender las siguientes eventualidades:<br>
    b) SERVICIOS EN INSTALACIONES ELECTRICAS
    Restablecimiento del servicio de energía eléctrica por causa de un corte total o parcial no proveniente
    de la empresa distribuidora de energía; cuando la falla se origine en el interior del domicilio residencial
    del cliente.<br>
    Reparación o cambio de tableros eléctricos, llaves térmicas, interruptores (disyuntores) o fusibles
    dañados a causa de corte total o parcial de energía; sólo en aquellas partes que pertenezcan a la
    instalación eléctrica del domicilio residencial.<br>
    Exclusiones particulares:<br>
    No incluye la reparación de ningún aparato o equipo que funcione con energía eléctrica, ni reposición de
    accesorios (lámparas, luminarias, balastros, etc.).<br>
    c) SERVICIOS DE PLOMERIA.<br>
    Se consideran dentro de estos servicios las fallas por rotura o pérdida en las instalaciones hidráulicas y
    sanitarias que requieran reparación de urgencia.
    Quedan incluidas cañerías internas, flexibles de canillas y bidet, fuelle de inodoros, llaves de paso.<br>
    Exclusiones particulares:<br>
    - Reparación de artefacto, equipo u accesorio que se encuentre conectados a las instalaciones
    hidráulicas y sanitarias (lavatorios, bidet, inodoro, lavarropas, termo tanques, calefones, griferías,
    canillas, cueritos, válvulas, rosca, mochila de baño empotrada o externa, flotantes, etc.).<br>
    - Las reparaciones de daños por filtración o humedad que sean consecuencia de fugas en tuberías y
    llaves.<br>
    d) SERVICIOS DE GAS<br>
    En caso de rotura de cañerías, llaves u otras instalaciones externas o a la vista en el domicilio residencial
    del cliente, que implique riesgo en caños externos de entrada y/ o salida de gas, se enviará a un técnico
    especializado para que haga una verificación y elimine la fuga.
    No están incluidas aquellas reparaciones que sean responsabilidad de la empresa que suministra el gas.<br>
    No están incluidos los artefactos estufas, calefones, cocinas, etc. que estén conectados a la red de gas.<br>
    e) SERVICIOS DE CERRAJERIA<br>
    Reparación y/o apertura de cerraduras dañadas por avería, accidente o roba en la puerta principal que
    se encuentre trabada y que impidan el ingreso o egreso al domicilio residencial, y/o que atente contra la
    seguridad de este.<br>
    En caso de que se requiera un remplazo de cerradura, Experta Asistencia conserva el derecho de
    retener la cerradura averiada una vez que la remplazó por una en funcionamiento.<br>
    Exclusiones particulares: La fabricación de llaves duplicadas de cualquier tipo.<br>
    f) SERVICIOS DE VIDRIERIA<br>
    Cambio de vidrios rotos en puertas y ventanas externas y siempre y cuando la no reposición de estos
    atente contra la seguridad del domicilio residencial.<br>
    Exclusiones particulares:<br>
    Vidrios de puertas internas, espejos, vitroaux, tragaluces, claraboyas (vidrios horizontales)
    g) MANTENIMIENTO DE AIRE ACONDICIONADO y ESTUFAS<br>
    Limitado a $ 2.000 por evento y 1 evento anual.
    La prestación cubre el mantenimiento en general y, en el caso de aire acondicionado, la recarga de gas
    hasta el límite informado.<br>
    h) SERVICIO GENERAL DE MANTENIMIENTO<br>
    Limitado a $ 1.500, hasta 1 evento anual, por cada servicio.
    Experta Asistencia brindará los siguientes servicios: instalación de luminarias, destape de cañerías,
    instalación de grifos, reparación de filtraciones de humedad, trabajos generales de albañilería e
    instalación de cortinas.<br>
    i) REEMBOLSO DE GASTOS POR ALQUILER DE GRUPO ELECTROGENO<br>
    Tope 1 evento hasta 2 días, $2.000 tope total.<br>
    Servicio de conexión para trabajos generales en domicilio<br>
    Costo preferente<br>
    j) MANTENIMIENTO GENERAL<br>
    Tope: 1 evento por $2.000 cada uno.<br>
    Experta Asistencia brindará los siguientes servicios: Plomería y Cerrajería<br>
    k) PRESTACIONES PROGRAMADAS – SERVICIO DE CONEXION<br>
    Sin límites
    Experta Asistencia enviará un prestador al domicilio residencial del cliente a resolver aquellas
    eventualidades que no estén cubiertas por los servicios detallados.
    Los costos de estos prestadores así como los repuestos y materiales correrán por cuenta del cliente.
    Condiciones particulares:
    Experta Asistencia podrá referenciar los especialistas dentro de un lapso de 48 Hs. de requerido el
    servicio.<br>
    El trabajo realizado entre el cliente y el prestador será de común acuerdo entre las partes y no
    existirá ninguna responsabilidad de Experta Asistencia.<br>
    l) EN CASO DE EVENTO GRAVE
    En caso de producirse un evento grave en el domicilio residencial del cliente, el cual se volviera
    inhabitable, Experta Asistencia brindará los siguientes servicios:<br>
    - Seguridad y vigilancia. Hasta 2 eventos anuales, limitado a $ 3.500 por evento<br>
    - Traslado y guarda de muebles en caso de inhabitabilidad. Hasta 1 evento anual, limitado a $5.000
    - Reembolso de gastos de hospedaje y alimentos en caso de inhabitabilidad. Hasta 1 evento anual,
    limitado a $ 4.000 por día, hasta 3 días.<br>
    2. OBLIGACIONES DEL CLIENTE:<br>
    El cliente se obliga bajo el presente a:<br>
    1. Brindar información completa y veraz en todos los casos.<br>
    2. Estar presente en cualquiera de los casos derivados de un servicio de asistencia.<br>
    3. Comunicarse con Experta Asistencia lo más rápido posible para solicitar los servicios.<br>
    4. En caso de que Experta Asistencia haya aprobado un reintegro al cliente, el mismo posee 30 días
    corridos para enviar la documentación solicitada y/o comprobantes originales. Vencido ese plazo,
    Experta Asistencia no procederá al reintegro.<br>
    5. En caso que el cliente no se haya podido comunicar con Experta Asistencia por problemas ajenos a
    él, el cliente debe comunicarse dentro de las 24 Hs para dar aviso de lo ocurrido y gestionar su
    reintegro.<br>
    3. EXCLUSIONES GENERALES.<br>
    Experta Asistencia no estará obligado a la prestación de los servicios en las siguientes situaciones:<br>
    a. Cuando el cliente no se identifique como tal.<br>
    b. Cuando el cliente incumpla cualesquiera de las obligaciones indicadas en este
    condicionado.<br>
    c. Cuando los servicios sean solicitados como consecuencia directa o indirecta de huelgas,
    guerra, invasión, actos de enemigos extranjeros, hostilidades (se haya declarado la guerra
    o no), rebelión, guerra civil, insurrección, terrorismo, pronunciamientos, manifestaciones,
    movimientos populares, radioactividad o cualquier otra causa de fuerza mayor.<br>
    Para el servicio de asistencia hogar quedan excluidos:<br>
    a) Cualquier daño preexistente al momento de contratación de los servicios.<br>
    b) La prestación del servicio en un domicilio diferente al declarado por el cliente como domicilio
    residencial.<br>
    c) Cuando la prestación del servicio se solicite para espacios que pertenezcan a las áreas comunes
    de los edificios, propiedades horizontales o viviendas.<br>
    d) La colocación de recubrimiento final en pisos, paredes o techos, tales como: lozas, mosaicos,
    mármol, tapiz, pintura, materiales de barro o acabados de madera.<br>
    e) La reparación de aparatos o equipo eléctricos que resulten dañados a consecuencia de una falla
    eléctrica en las instalaciones del hogar.<br>
    f) Las reparaciones de equipos conectados a las tuberías de agua como calderas, calentadores, aire
    acondicionado, lavadoras o secadoras.<br>
    g) Los daños que sean consecuencia de sismo, inundación, erupción volcánica, incendio y cualquier
    fenómeno natural.<br>
    h) Las reparaciones de daños causados en los bienes que sean consecuencia de una falla en los
    servicios de energía eléctrica, hidráulicos y sanitarios.<br>
    i) Cuando por orden de alguna autoridad competente se impida la ejecución de los trabajos a
    realizar. Cuando el personal de cualquier autoridad oficial con orden de embargo, allanamiento,
    aseguramiento de bienes, aprehensión, cateo, investigación, rescate, se vea obligada a forzar,
    destruir o romper cualquier elemento de acceso como son: puertas, ventanas, chapas,
    cerraduras en el domicilio residencial del cliente.`,
  },
  "asistencias-en-via-publica-arg": {
    html: `<strong>ASISTENCIA VÍA PÚBLICA</strong><br>
    1. DESCRIPCIÓN DE LOS SERVICIOS<br>
    Los servicios son prestados por Experta Asistencia SAU CUIT 30-71671840-5<br>
    Para solicitar un servicio, el cliente deberá comunicarse al número telefónico 0800 222 0668.<br>
    Servicio operativo las 24 horas del día, los 365 días del año.<br>
    <strong>DEFINICIONES</strong><br>
    Para los efectos de la presentación de los servicios aquí detallados, se entenderá por:<br>
    a) CLIENTE: Titular contratante del servicio.<br>
    b) SERVICIOS: Son las actividades, operaciones y funciones a cargo de Experta Asistencia relacionadas
    con la asistencia, y cuya descripción, límites, alcances y condiciones se detallan en este documento.<br>
    c) PRESTADOR: Experta Asistencia SAU<br>
    d) EVENTO: Solución brindada por el prestador en relación con una solicitud de servicio. En caso de
    que el cliente solicite una segunda solución para resolver la misma eventualidad, el proveedor
    podrá considerarlo como un evento adicional. Los plazos son por año calendario.<br>
    e) ACCIDENTE: Suceso imprevisto que altera la marcha normal o prevista de las cosas, causando daños
    a una persona y/u objeto.<br>
    f) URGENCIA: Es la aparición fortuita en cualquier lugar o actividad de un problema de causa diversa y
    gravedad variable que genera la conciencia de una necesidad inminente de atención, por parte del
    sujeto.<br>
    g) EMERGENCIA: Es aquella situación urgente que pone en peligro inmediato la vida de la persona. En
    esta situación se requiere una asistencia inmediata
    h) AVERIA: Daño o deterioro de un objeto de modo tal que su funcionamiento normal esté impedido.<br>
    i) ROBO: Usurpación ilegítima utilizando la fuerza sobre las cosas, amenazas o violencia física en las
    personas.<br>
    j) REINTEGRO: Monto de dinero a entregar al cliente cuando hubiera abonado gastos en relación con
    los servicios cubiertos aquí descriptos, siempre y cuando los mismos hayan sido autorizados
    debidamente por el prestador antes de ser realizados. Los reintegros se realizarán por el proveedor
    en cuenta bancaria a designar por el cliente titular.<br>
    k) EXCEDENTE: Monto a pagar por parte del cliente cuando se supere el tope económico establecido
    en la cobertura al momento de recibir el servicio.<br>
    l) IDENTIFICACIÓN COMO CLIENTE: La identificación será necesaria al momento de ser solicitada la
    asistencia. En todos los casos, el beneficiario deberá suministrar la siguiente información:<br>
    Nombre y apellido<br>
    Número DNI<br>
    Motivo del llamado<br>
    Número telefónico para eventual contacto<br>
    m) VALIDEZ TERRITORIAL: Los servicios se brindarán en la República Argentina.<br>
    <strong>SERVICIOS</strong><br>
    a) Código Rojo:<br>
    Tope: Sin límite<br>
    En caso de accidente o robo se enviará con la mayor prontitud el servicio de ambulancia código rojo
    para atender a los clientes y trasladar en caso de ser necesario hasta el centro médico más cercano.<br>
    b) Traslado en taxi o Remis:<br>
    Tope: 1 evento. $1.500 por evento.<br>
    El desplazamiento para el traslado del cliente hasta el domicilio habitual del titular o dependencia
    policial más cercana, según lo que el cliente considere más conveniente
    c) Transmisión de mensajes urgentes:<br>
    Tope: Ilimitado<br>
    Experta Asistencia se encargará de transmitir los mensajes urgentes y justificados de los titulares,
    relativos a cualquiera de los eventos objeto de las prestaciones de esta modalidad.<br>
    d) Asistencia legal ante robo de documentación personal:<br>
    Tope: Ilimitado<br>
    Por accidente o robo.<br>
    Experta Asistencia brindará asesoramiento y evacuación de consultas de cualquier índole legal a los
    clientes, en forma telefónica, según el caso. Las consultas serán contestadas a la brevedad posible por
    profesionales especializados en cada área del derecho. <br>
    Se aclara que la asistencia legal no constituye un tipo de asistencia por sí misma, sino que es parte
    componente de la asistencia
    Estos servicios se limitan al servicio contratado y no son de libre interpretación.<br>
    e) Cambio de cerraduras y reposición de llaves por robo.<br>
    Tope: 1 evento. $1.500 por evento.<br>
    En el caso de robo del bolso o cartera personal del titular que implique la pérdida de las llaves del
    domicilio personal.<br>
    f) Reembolso de gastos de documentos:<br>
    Tope: 1 evento. $2.000 por evento.<br>
    Experta Asistencia sufragará los gastos por tramitación y reposición de documentación.<br>
    2. CONSIDERACIONES LEGALES<br>
    Subrogación<br>
    Hasta la concurrencia de las sumas desembolsadas en cumplimiento de las obligaciones emanadas de
    las presentes condiciones generales, Experta Asistencia quedará automáticamente subrogada en los
    derechos y acciones que puedan corresponder a los clientes o sus herederos contra terceras personas
    físicas o jurídicas en virtud del evento ocasionante de la asistencia prestada.<br>
    Se establecerá que cuando las prestaciones determinadas en estas condiciones generales están también
    cubiertas total o parcialmente por una póliza de seguros o cualquier otro medio, el cliente se obligará a
    efectuar todas las gestiones y reclamos necesarios ante la compañía de seguros o terceros que
    correspondieren, para lograr que éstos respondan directamente y en primer término por el
    cumplimiento de las obligaciones respectivas.<br>
    En consecuencia, el cliente cede irrevocablemente a favor de Experta Asistencia los derechos y acciones
    comprendidos en la presente cláusula, obligándose a llevar a cabo la totalidad de los actos jurídicos que
    a tal efecto resulten necesarios y a prestar toda la colaboración o a subrogar tales derechos a Experta
    Asistencia, esta última quedará automáticamente desobligada a abonar los gastos de asistencia
    originados.<br>
    Responsabilidad<br>
    Experta Asistencia no será responsable y no indemnizará a los clientes por cualquier daño, perjuicio,
    lesión o enfermedad por el hecho de haberle brindado al cliente a su solicitud, personas o profesionales
    para que los asistieren por cualquiera de los servicios previstos en las presentes condiciones generales,
    limitando su responsabilidad a lo expresado bajo las mismas.
    Experta Asistencia provee únicamente servicios cuando le son solicitados, y en las circunstancias
    previstas en las presentes condiciones.<br>
    Ley y jurisdicción aplicable<br>
    Deberá dejarse constancia que para todas las cuestiones de derecho relativas a la relación entre los
    clientes de los servicios detallados precedentemente quedará pactada la aplicación de la legislación
    argentina y la jurisdicción de los tribunales ordinarios de la ciudad de Buenos Aires, con renuncia a
    cualquier otro fuero o jurisdicción.<br>
    Reserva<br>
    Experta Asistencia se reserva el derecho a exigir a los clientes el reembolso de cualquier gasto efectuado
    indebidamente, en caso de haberse prestado servicios no contratados, y/o fuera del período de vigencia
    del acuerdo establecido y/o en forma diferente a todo lo precedentemente indicado.<br>
    3. OBLIGACIONES DEL CLIENTE:<br>
    El cliente se obliga bajo el presente a:<br>
    a) Brindar información completa y veraz en todos los casos.<br>
    b) Estar presente en cualquiera de los casos derivados de un servicio de asistencia.<br>
    c) Comunicarse con Experta Asistencia lo más rápido posible para solicitar los servicios.<br>
    d) En caso de que Experta Asistencia haya aprobado un reintegro al cliente, el mismo posee 30 días
    corridos para enviar la documentación solicitada y/o comprobantes originales. Vencido ese plazo,
    Experta Asistencia no procederá al reintegro.<br>
    e) En caso que el cliente no se haya podido comunicar con Experta Asistencia por problemas ajenos a
    él, el cliente debe comunicarse dentro de las 24 Hs para dar aviso de lo ocurrido y gestionar su
    reintegro.<br>
    4. EXCLUSIONES GENERALES.<br>
    Experta Asistencia no estará obligado a la prestación de los servicios en las siguientes situaciones:<br>
    a. Cuando el cliente no se identifique como tal.<br>
    b. Cuando el cliente incumpla cualesquiera de las obligaciones indicadas en este
    condicionado.<br>
    c. Cuando los servicios sean solicitados como consecuencia directa o indirecta de
    huelgas, guerra, invasión, actos de enemigos extranjeros, hostilidades (se haya
    declarado la guerra o no), rebelión, guerra civil, insurrección, terrorismo,
    pronunciamientos, manifestaciones, movimientos populares, radioactividad o
    cualquier otra causa de fuerza mayor.`,
  },
};


export default terms;
