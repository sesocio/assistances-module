import store from '@/store';

export default [
  {
    path: "/asistencias/:title_id",
    name: "assistance_detail",
    component: () => import(/* webpackChunkName: "Detail" */ '../pages/Index'),
    title: "SeSocio",
    meta: { requiresAuth: true },
    async beforeEnter(to, from, next) {
      if(Object.keys(store.state.user.current).length === 0) {
        await Promise.all([store.dispatch('user/getCurrent')]);
      }

      let current = store.state.user.current;
      let banned_types = current.banned_type;
      let country = current.country;
      
      if(banned_types && banned_types[0] === 'totally_banned') next('/asistencias/completar-documentacion');
      if(country && country !== 'AR') next('/asistencias/no-disponible');
      next();
    }
  },
  {
    path: "/asistencias/:title_id/pago",
    name: "assistance_payment",
    component: () => import(/* webpackChunkName: "PaymentForm" */ '../pages/PaymentForm'),
    title: "SeSocio",
    meta: { requiresAuth: true }
  },
  {
    path: "/asistencias/:title_id/resumen",
    name: "assistance_resume",
    component: () => import(/* webpackChunkName: "Resume" */ '../pages/Resume'),
    title: "SeSocio",
    meta: { requiresAuth: true }
  },
  {
    path: "/asistencias/:title_id/metodos-de-pago",
    name: "assistance_methods",
    component: () => import(/* webpackChunkName: "Methods" */ '../pages/Methods'),
    title: "SeSocio",
    meta: { requiresAuth: true }
  },
  {
    path: "/asistencias/:title_id/pendiente",
    name: "assistance_pending",
    component: () => import(/* webpackChunkName: "Summary" */ '../pages/Summary'),
    title: "SeSocio",
    meta: { requiresAuth: true }
  },
  {
    path: "/asistencias/:title_id/aprobado",
    name: "assistance_approved",
    component: () => import(/* webpackChunkName: "Summary" */ '../pages/Summary'),
    title: "SeSocio",
    meta: { requiresAuth: true }
  },
  {
    path: "/asistencias/:title_id/rechazado",
    name: "assistance_rejected",
    component: () => import(/* webpackChunkName: "Summary" */ '../pages/Summary'),
    title: "SeSocio",
    meta: { requiresAuth: true }
  },
  {
    path: "/asistencias/:title_id/credencial",
    name: "assistance_credential",
    component: () => import(/* webpackChunkName: "Credential" */ '../pages/Credential'),
    title: "SeSocio",
    meta: { requiresAuth: true }
  },
  {
    path: "/asistencias/:title_id/terminos",
    name: "assistance_terms",
    component: () => import(/* webpackChunkName: "Terms" */ '../pages/Terms'),
    title: "SeSocio"
  }
];
