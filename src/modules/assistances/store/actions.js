import Axios from 'axios';

const getAssistences = async ({ commit, state, dispatch }) => {
  try {
    let url = process.env.VUE_APP_API_ASSISTANCE_URL + process.env.VUE_APP_API_ASSISTANCE_VERSION_URL +
      `/assistances`;

    await Axios.get(url).then(function (resp) {
      commit("SET_ASSISTANCES", resp.data);
    }).catch(function (e) {
      console.log(false);
    });

  } catch (err) {
    return err;
  }
};

const getAssistence = async ({ commit, state, dispatch }, payload) => {
  if (payload) {
    return new Promise(async (resolve, reject) => {
      try {
        commit("SET_ASSISTANCE", '');
        commit("SET_LOADER_ASSISTANCES", true);
        let url = process.env.VUE_APP_API_ASSISTANCE_URL + process.env.VUE_APP_API_ASSISTANCE_VERSION_URL +
          `/assistances/${payload}`;
        const response = await Axios.get(url)
        commit("SET_ASSISTANCE", response.data);
        commit("SET_LOADER_ASSISTANCES", false);
        resolve(response)

      } catch (e) {
        commit("SET_LOADER_ASSISTANCES", false);
        reject(e)
      }
    })
  }
};

const setAssistancePersonalForm = async ({ commit, state, dispatch }, payload) => {
  commit("SET_PERSONAL_INFO_BUY_FORM", payload);
};

const setAssistancePayment = async ({ commit, state, dispatch }, payload) => {
  commit("SET_PAYMENT_BUY_FORM", payload);
};

const getUserBalanceAvailable = async ({ commit, dispatch, state }) => {
  commit("SET_USER_CURRENCIES_LOADER", true);
  dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  //let url = process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/withdrawals/get_withdrawal_options";
  let projectId = 387;
  let url = process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + '/portfolios?filter[project_id]=' + projectId;

  try {
    commit("SET_USER_IARS_BALANCE", null);
    let resp = await Axios({ url: url, method: "GET" });
    let available = 0;
    let json_data = resp.data;

    for (let i = 0; i < json_data.data.length; i++) {
      if (json_data.data[i].hasOwnProperty('attribute') != undefined) {
        if (json_data.data[i].attributes.project_id == 387) {
          available = json_data.data[i].attributes.parts_available;
        }
      }
    }

    commit("SET_USER_IARS_BALANCE", available);
    commit("SET_USER_CURRENCIES_LOADER", false);
    dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  } catch (error) {
    commit("SET_USER_CURRENCIES_LOADER", false);
    return error.response;
  }
};

const getAmountUserAvailable = async ({ commit, dispatch }, payload) => {

  // let url = process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + '/portfolios?filter[project_id]=' + projectId;
  let url = process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + '/users/balances?id=' + payload.userId;
  try {
    commit("SET_USER_IARS_AMOUNT", null);
    let resp = await Axios({ url: url, method: "GET" });
    let available = 0;
    let json_data = resp.data;

    for (let i = 0; i < json_data.length; i++) {
      if (json_data[i].name === 'IARS') {   
          available = json_data[i].available;
      }
    }
    commit("SET_USER_IARS_AMOUNT", parseFloat(available).toFixed(2));
  } catch (error) {
    return error.response;
  }
};

const buyAssistance = async ({ commit, dispatch }, payload) => {
  dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  // let endpoint = payload.paymentMethod === 'mercado_pago' ? 'buyAssistanceMP' : 'buyAssistance'
  let endpoint = 'buyAssistance';
  if (payload.paymentMethod === 'mercado_pago') {
    endpoint = 'buyAssistanceMP'
  } else if (payload.paymentMethod === 'mercado_pago_token') {
    endpoint = 'buyAssistanceMPToken'
  }
  let url = process.env.VUE_APP_API_ASSISTANCE_URL + process.env.VUE_APP_API_ASSISTANCE_VERSION_URL + '/' + endpoint;
  let data = {
    user_assistance: {
      assistance_id: payload.assistanceId,
      available: payload.available,
      payment_method: payload.paymentMethod,
      quantity: payload.quantity_assists,
      recurrent_payment: payload.recurrent_payment,
      user_id: payload.userId,      
    }
  };
  console.log(data);
  try {
    await Axios({ url: url, method: 'POST', data: data }).then(resp => {
      commit("SET_BUY_API_RESPONSE", resp.data);
    }).catch(function (e) {
      console.log(false);
    });
  } catch (err) {
    console.log(err);
  }
  dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
};

export default {
  getAssistences,
  getAssistence,
  setAssistancePersonalForm,
  setAssistancePayment,
  getUserBalanceAvailable,
  buyAssistance,
  getAmountUserAvailable,
};
