const SET_ASSISTANCES = (state, payload) => {
  state.assistances = payload;
};

const SET_ASSISTANCE = (state, payload) => {
  state.assistance = payload;
};

const SET_LOADER_ASSISTANCES = (state, payload) => {
  state.loaderAssistances = payload;
};

const SET_PERSONAL_INFO_BUY_FORM = (state, payload) => {
  state.buyForm.personal_info = payload;
};

const SET_PAYMENT_BUY_FORM = (state, payload) => {
  state.buyForm.payment = payload;
};

const SET_USER_CURRENCIES = (state, payload) => {
  state.user_currencies = payload;
};

const SET_USER_CURRENCIES_LOADER = (state, payload) => {
  state.user_currencies_loader = payload;
};

const SET_BUY_API_RESPONSE = (state, payload) => {
  state.buyApiResponse = payload;
};

const SET_USER_IARS_BALANCE = (state, payload) => {
  state.user_balance_available = payload;
};

const SET_USER_IARS_AMOUNT = (state, payload) => {
  state.user_amount_available = payload;
};

export default {
  SET_ASSISTANCES,
  SET_ASSISTANCE,
  SET_LOADER_ASSISTANCES,
  SET_PERSONAL_INFO_BUY_FORM,
  SET_PAYMENT_BUY_FORM,
  SET_USER_CURRENCIES,
  SET_USER_CURRENCIES_LOADER,
  SET_BUY_API_RESPONSE,
  SET_USER_IARS_BALANCE,
  SET_USER_IARS_AMOUNT
};
