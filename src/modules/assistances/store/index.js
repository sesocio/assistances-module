import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

const state = {
  loaderAssistances: false,
  assistances: [],
  assistance: {},
  buyForm: {
    personal_info: "",
    payment: ""
  },
  user_currencies: [],
  user_currencies_loader: false,
  user_balance_available: null,
  buyApiResponse: null,
  user_amount_available: null,
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
};
