export default [
  {
    path: "/asistencias",
    name: "assistance_home",
    component: () => import(/* webpackChunkName: "Home" */ '../pages/Home'),
    title: "SeSocio"
  },
  {
    path: "/asistencias/completar-documentacion",
    name: "assistance_user_blocked",
    component: () => import(/* webpackChunkName: "UserBlocked" */ '../pages/UserBlocked'),
    title: "SeSocio"
  },
  {
    path: "/asistencias/no-disponible/:title_id",
    name: "assistance_user_no_access",
    component: () => import(/* webpackChunkName: "UserNoAccess" */ '../pages/UserNoAccess'),
    title: "SeSocio"
  },
];
